<?php

namespace OneOfZero\Router;

use InvalidArgumentException;
use OneOfZero\Router\Dispatcher\StrategyInterface;

class Route
{
	const GET = 'GET';
	const POST = 'POST';
	const PUT = 'PUT';
	const DELETE = 'DELETE';
	const HEAD = 'HEAD';
	const OPTIONS = 'OPTIONS';
	
	const ALL = [ 'GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'OPTIONS' ];

	/**
	 * @var string $name
	 */
	private $name;
	
	/**
	 * @var string[] $methods
	 */
	private $methods = [ self::GET ];

	/**
	 * @var string $routePattern
	 */
	private $routePattern;

	/**
	 * @var mixed $target
	 */
	private $target;

	/**
	 * @var StrategyInterface|string $strategy
	 */
	private $strategy;

	/**
	 * @param string|null $name
	 * @param StrategyInterface|string $strategy
	 */
	public function __construct($name = null, $strategy = null)
	{
		$this->name = $name;
		$this->strategy = $strategy;
	}

	/**
	 * @param mixed $target
	 *
	 * @return self
	 */
	public function to($target)
	{
		$this->target = $target;
		return $this;
	}

	/**
	 * @param StrategyInterface|string $strategy
	 * 
	 * @return self
	 */
	public function using($strategy)
	{
		if (is_object($strategy) && !($strategy instanceof StrategyInterface))
		{
			throw new InvalidArgumentException('Objects provided to using() must implement StrategyInterface');
		}
		
		if (!is_object($strategy) && !is_string($strategy))
		{
			throw new InvalidArgumentException('The argument for method() must be a StrategyInterface instance or a string');
		}
		
		$this->strategy = $strategy;
		return $this;
	}

	/**
	 * @param string $method
	 *
	 * @return self
	 */
	public function method($method)
	{
		if (!is_string($method))
		{
			throw new InvalidArgumentException('The argument for method() must be a string');
		}
		
		$this->methods = [ $method ];
		return $this;
	}

	/**
	 * @param string[] $methods
	 *
	 * @return self
	 */
	public function methods(array $methods)
	{
		foreach ($methods as $method)
		{
			if (!is_string($method))
			{
				throw new InvalidArgumentException('The argument for methods() must be a string array');
			}
		}
		
		$this->methods = $methods;
		return $this;
	}

	/**
	 * @param string $routePattern
	 *
	 * @return self
	 */
	public function route($routePattern)
	{
		if (!is_string($routePattern))
		{
			throw new InvalidArgumentException('The argument for route() must be a string');
		}
		
		$this->routePattern = $routePattern;
		return $this;
	}

	/**
	 * @param string $pattern
	 * 
	 * @return self
	 */
	public function get($pattern)
	{
		return $this->method(self::GET)->route($pattern);
	}

	/**
	 * @param string $pattern
	 *
	 * @return self
	 */
	public function post($pattern)
	{
		return $this->method(self::POST)->route($pattern);
	}

	/**
	 * @param string $pattern
	 *
	 * @return self
	 */
	public function put($pattern)
	{
		return $this->method(self::PUT)->route($pattern);
	}

	/**
	 * @param string $pattern
	 *
	 * @return self
	 */
	public function delete($pattern)
	{
		return $this->method(self::DELETE)->route($pattern);
	}

	/**
	 * @param string $pattern
	 *
	 * @return self
	 */
	public function head($pattern)
	{
		return $this->method(self::HEAD)->route($pattern);
	}

	/**
	 * @param string $pattern
	 *
	 * @return self
	 */
	public function options($pattern)
	{
		return $this->method(self::OPTIONS)->route($pattern);
	}
}
