<?php

namespace OneOfZero\Router\Dispatcher;

use Closure;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class CallableStrategy extends AbstractStrategy
{
	/**
	 * {@inheritdoc}
	 */
	public function dispatch($target, array $variables, ServerRequestInterface $request, ResponseInterface $response)
	{
		return $this->invokeCallable($target, $variables, $request, $response);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function validate($target)
	{
		// Support for normal resolvable callables
		if ($target instanceof Closure || is_callable($target))
		{
			return true;
		}

		// Support for 'container_key::method'
		if (is_string($target) && strpos($target, '::') !== false)
		{
			$target = explode('::', $target);
		}

		// Left-over must be syntactically correct callable array
		if (!is_callable($target, true))
		{
			return false;
		}

		// Support for [ 'container_key', 'method' ]
		if (is_string($target[0]) && $this->container !== null && $this->container->has($target[0]))
		{
			return true;
		}

		// Method in callable is invalid
		return false;
	}
}
