<?php

namespace OneOfZero\Router\Dispatcher;

use Closure;
use InvalidArgumentException;
use OneOfZero\Router\Exceptions\NotInvocableException;
use Psr\Http\Message\RequestInterface;
use Interop\Container\ContainerInterface;
use OneOfZero\Router\Exceptions\HttpException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;

abstract class AbstractStrategy implements StrategyInterface
{
	/**
	 * @var ContainerInterface $container
	 */
	protected $container;

	/**
	 * @param ContainerInterface $container
	 */
	public function setContainer(ContainerInterface $container)
	{
		$this->container = $container;
	}

	/**
	 * @param mixed $target
	 * @param array $variables
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * 
	 * @return ResponseInterface
	 * 
	 * @throws NotInvocableException
	 * @throws HttpException
	 */
	protected function invokeCallable($target, array $variables, ServerRequestInterface $request, ResponseInterface $response)
	{
		$target = $this->resolveCallable($target);

		if ($target instanceof Closure)
		{
			$reflector = new ReflectionFunction($target);
		}
		else
		{
			list($object, $method) = $target;
			$reflector = new ReflectionMethod($object, $method);
		}

		$parameters = [];

		foreach ($reflector->getParameters() as $parameter)
		{
			$typeHint = $parameter->getClass();

			if ($typeHint && $typeHint->implementsInterface(RequestInterface::class))
			{
				$parameters[] = $request;
			}
			elseif ($typeHint && $typeHint->implementsInterface(ServerRequestInterface::class))
			{
				$parameters[] = $request;
			}
			elseif ($typeHint && $typeHint->implementsInterface(ResponseInterface::class))
			{
				$parameters[] = $response;
			}
			if (is_array($parameter))
			{
				$parameters[] = $variables;
			}
			elseif (array_key_exists($parameter->name, $variables))
			{
				$parameters[] = $variables[$parameter->name];
			}
			elseif ($parameter->isDefaultValueAvailable())
			{
				$parameters[] = $parameter->getDefaultValue();
			}
			elseif ($parameter->allowsNull())
			{
				$parameters[] = null;
			}
			else
			{
				throw new NotInvocableException($target instanceof Closure
					? "Cannot provide value for parameter {$parameter->name} in closure"
					: "Cannot provide value for parameter {$parameter->name} on method {$reflector->name} in class {$reflector->getDeclaringClass()->name}"
				);
			}
		}

		return call_user_func_array($target, $parameters);
	}

	/**
	 * @param Closure|callable|array|string $target
	 *
	 * @return Closure|callable
	 */
	protected function resolveCallable($target)
	{
		if ($target instanceof Closure)
		{
			return $target;
		}

		if (is_string($target))
		{
			// 'container_key::method' or 'class_name::method'
			$target = explode('::', $target);
		}

		list($class, $method) = $target;
		
		if (is_string($class))
		{
			if ($this->container !== null && $this->container->has($class))
			{
				// [ 'container_key', 'method' ]
				$target = [ $this->container->get($class), $method ];
			}
			elseif (class_exists($class))
			{
				// [ 'class_name', 'method' ]
				$reflector = new ReflectionClass($class);
				$constructor = $reflector->getConstructor();

				// Ensure that the class can be instantiated safely
				if ($constructor === null || $constructor->getNumberOfRequiredParameters() === 0)
				{
					$target = [ $reflector->newInstance(), $method ];
				}
			}
		}
		
		if (is_callable($target))
		{
			return $target;
		}

		throw new InvalidArgumentException('Provided target is not a valid callable');
	}
}
