<?php

namespace OneOfZero\Router\Dispatcher;

use OneOfZero\Router\Exceptions\HttpNotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;

class ActionControllerStrategy extends CallableStrategy
{
	/**
	 * {@inheritdoc}
	 */
	public function dispatch($target, array $variables, ServerRequestInterface $request, ResponseInterface $response)
	{
		$controller = $this->resolveTarget(array_key_exists('controller', $variables)
			? $variables['controller']
			: $target
		);
		
		$action = array_key_exists('action', $variables) 
			? $variables['action'] 
			: 'index'
		;
		
		$actionMethod = strtolower($request->getMethod()) . ucfirst($action);
		
		if ($controller === null 
		|| !($controller instanceof ActionControllerInterface) 
		|| !method_exists($controller, $actionMethod))
		{
			throw new HttpNotFoundException();
		}
		
		if (method_exists($controller, '__beforeAction'))
		{
			$response = $this->invokeCallable([ $controller, '__beforeAction' ], $variables, $request, $response);
		}
		
		$response = $this->invokeCallable([ $controller, $actionMethod ], $variables, $request, $response);
		
		if (method_exists($controller, '__afterAction'))
		{
			$response = $this->invokeCallable([ $controller, '__afterAction' ], $variables, $request, $response);
		}
		
		return $response;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function validate($target)
	{
		if (is_object($target))
		{
			return true;
		}

		if (!is_string($target))
		{
			return false;
		}

		if ($this->container !== null && $this->container->has($target))
		{
			return true;
		}

		if (class_exists($target))
		{
			$constructor = (new ReflectionClass($target))->getConstructor();
			return $constructor !== null && $constructor->getNumberOfRequiredParameters() === 0;
		}
		
		return class_exists($target);
	}

	/**
	 * @param object|string $target
	 * 
	 * @return object
	 */
	private function resolveTarget($target)
	{
		if ($target === null || is_object($target))
		{
			return $target;
		}
		
		if ($this->container !== null && $this->container->has($target))
		{
			return $this->container->get($target);
		}

		if (class_exists($target))
		{
			$reflector = new ReflectionClass($target);
			$constructor = $reflector->getConstructor();
			
			 if ($constructor !== null && $constructor->getNumberOfRequiredParameters() === 0)
			 {
				 return $reflector->newInstance();
			 }
		}
		
		return null;
	}
}
