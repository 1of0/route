<?php

namespace OneOfZero\Router\Dispatcher;

use OneOfZero\Router\Exceptions\HttpException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface StrategyInterface
{
	/**
	 * @param mixed $target
	 * @param array $variables
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * 
	 * @return ResponseInterface
	 * 
	 * @throws HttpException
	 */
	public function dispatch($target, array $variables, ServerRequestInterface $request, ResponseInterface $response);
	
	/**
	 * @param mixed $target
	 *
	 * @return bool
	 */
	public function validate($target);
}
