<?php

namespace OneOfZero\Router;

use InvalidArgumentException;
use OneOfZero\Router\Dispatcher\StrategyInterface;

class RouteMapper
{
	/**
	 * @var Route[] $routes
	 */
	private $routes = [];
	
	/**
	 * @var StrategyInterface|string $defaultStrategy
	 */
	private $defaultStrategy;

	/**
	 * @var array $patternAliases
	 */
	private $patternAliases;
	
	/**
	 * @param Route $route
	 */
	public function add(Route $route)
	{
		$this->routes[] = $route;
	}
	
	/**
	 * @param string|null $name
	 * 
	 * @return Route
	 */
	public function map($name = null)
	{
		$route = new Route($name, $this->defaultStrategy);
		
		$this->add($route);
		
		return $route;
	}

	/**
	 * @param string $alias
	 * @param string $pattern
	 */
	public function addPatternAlias($alias, $pattern)
	{
		if (!is_string($alias) || !is_string($pattern))
		{
			throw new InvalidArgumentException('The arguments for addPatternAlias() must be two strings');
		}
		
		$this->patternAliases[$alias] = $pattern;
	}

	/**
	 * @return StrategyInterface|string
	 */
	public function getDefaultStrategy()
	{
		return $this->defaultStrategy;
	}

	/**
	 * @param StrategyInterface|string $defaultStrategy
	 */
	public function setDefaultStrategy($defaultStrategy)
	{
		$this->defaultStrategy = $defaultStrategy;
	}
}
