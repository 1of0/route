<?php

namespace OneOfZero\Router\Exceptions;

use Exception;

class NotInvocableException extends Exception
{
}
