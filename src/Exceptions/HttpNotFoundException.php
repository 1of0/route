<?php

namespace OneOfZero\Router\Exceptions;

class HttpNotFoundException extends HttpException
{
	public function __construct($statusCode = 404, $message = 'Not found')
	{
		parent::__construct($statusCode, $message);
	}
}
