<?php

namespace OneOfZero\Router\Exceptions;

use Exception;

class HttpException extends Exception
{
	public function __construct($statusCode, $message)
	{
		parent::__construct($message, $statusCode);
	}
}
