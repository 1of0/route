<?php

namespace OneOfZero\Router\Tests;

use OneOfZero\Router\Route;
use OneOfZero\Router\RouteMapper;
use OneOfZero\Router\Tests\Fixtures\RouteMapper\Controller;
use OneOfZero\Router\Tests\Fixtures\RouteMapper\CustomStrategy;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\ServerRequestInterface;

class RouteMapperTest extends PHPUnit_Framework_TestCase
{
	public function testTargets()
	{
		$mapper = new RouteMapper();
		
		// Closure
		$mapper->map()->get('/closure')->to(function () { return 'foo'; });
		
		// Callable
		$mapper->map()->get('/callable')->to([ $this, 'returnFoo' ]);
		
		// Controller
		$mapper->map()->get('/controller/{action}')->to(Controller::class);
		$mapper->map()->get('/controller/{action}')->to(new Controller());
	}

	public function testMethods()
	{
		$mapper = new RouteMapper();
		
		// Verbose GET
		$mapper->map()->method(Route::GET)->route('/verbose')->to([ $this, 'returnMethod' ]);

		// Shorthand GET
		$mapper->map()->get('/short-get')->to([ $this, 'returnMethod' ]);
		
		// Multiple methods
		$mapper->map()->methods([ Route::GET, Route::POST ])->route('/get-and-post')->to([ $this, 'returnMethod' ]);
		
		// All methods
		$mapper->map()->methods(Route::ALL)->route('/all-methods')->to([ $this, 'returnMethod' ]);
	}
	
	public function testStrategy()
	{
		$mapper = new RouteMapper();
		
		// Strategy class name
		$mapper->map()->get('/foo')->to([ $this, 'returnFoo' ])->using(CustomStrategy::class);
		
		// StrategyInterface instance
		$mapper->map()->get('/foo')->to([ $this, 'returnFoo' ])->using(new CustomStrategy());
	}
	
	public function returnFoo()
	{
		return 'foo';
	}
	
	public function returnMethod(ServerRequestInterface $request)
	{
		return $request->getMethod();
	}
}
