<?php

namespace OneOfZero\Router\Tests\Fixtures\RouteMapper;

use OneOfZero\Router\Dispatcher\StrategyInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class CustomStrategy implements StrategyInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function dispatch(ServerRequestInterface $request, ResponseInterface $response)
	{
		
	}
}
